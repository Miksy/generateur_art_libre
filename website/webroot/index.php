<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Générateur d'art libre</title>
	<link rel="stylesheet" href="/style.css">
</head>
<body>

	<nav class="nav">
		<a href="a-propos">à propos</a>
	</nav>
	<h1 class="site-title"><span>Générateur </span><span>d'art </span><span>libre</span></h1>

	<div class="imgs">
	<?php 
		$d = 'captures/';
		$filelist = glob($d.'*.{jpg,JPG,jpeg,JPEG,png,PNG}',GLOB_BRACE);
		foreach( array_reverse( $filelist ) as $file ) :
	?>
		<div class="img-container">
			<img src="<?= $file ?>" loading="lazy" width="1280" height="720" alt="">
		</div>
	<?php 
		endforeach;
	?>
	</div>

</body>
</html>
