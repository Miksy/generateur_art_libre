<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>À propos ▒▒ Générateur d'art libre</title>
	<link rel="stylesheet" href="/style.css">
</head>
<body>

	<nav class="nav">
		<a href="/">accueil</a>
	</nav>
	<h1 class="site-title"><span>À </span><span>Propos</span></h1>

	<div class="layout-pad">
		<div class="text-body">
			<p>
				Toutes les images générées avec ce dispositif sont sous <a href="https://artlibre.org/" target="_blank">Licence Art Libre</a>
			</p>
			<p>
				Le code source du dispositif ainsi que les informations nécessaires à sa recréation sont disponibles à cette adresse <a href="https://gitlab.com/Miksy/generateur_art_libre" target="_blank">https://gitlab.com/Miksy/generateur_art_libre</a>
			</p>
			<p>
				Auteur&nbsp;: Mika du Cybersuper<br>
				mika@cybersuper.space<br>
				<a href="https://cybersuper.space" target="_blank">https://cybersuper.space</a>
			</p>
		</div>
	</div>

</body>
</html>
