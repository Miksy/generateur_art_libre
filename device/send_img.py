import os, sys
import requests
import validators


if len(sys.argv) < 4:
    print("Not enough arguments")
    print("Usage: send_img.py <image_path> <url> <password>")
    sys.exit(1)


image_path = sys.argv[1]
url = sys.argv[2]
password = sys.argv[3]

if validators.url(url) is not True :
  print("invalid url")
  sys.exit(1)

if os.path.isfile(image_path) is False :
  print("image does not exist")
  sys.exit(1)

with open(image_path, 'rb') as img :
  name_img = os.path.basename(image_path)
  files = { 'very_cool_image': ( name_img, img, 'multipart/form-data', {'Expires': '0'} ) }
  data = { 'password': password }

  with requests.Session() as s :
    r = s.post( url, files=files, data=data )
    print(r.status_code)
    print(r.text)

sys.exit()