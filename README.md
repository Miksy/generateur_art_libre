# Générateur d'art libre


## Requis

### Dispositif

#### matériel
- ordinateur avec une carte graphique décente
- caméra / webcam \	il faut pouvoir récupérer le flux en direct
- un bouton pour déclencher la capture et son envoi au serveur


#### logiciel

- touchdesigner

- Python 3 \
		j'ai utilisé la même version que touchdesigner (3.7.2 au moment où j'écris ces lignes)

- modules python `python -m pip install *nom-du-module*`
	- requests
	- validators


### Serveur

Simple pages en php, j'aurais pu utiliser nodejs mais php est dispo sur la plupart des serveurs et l'installation est carrément plus simple pour les non initiés.


## Installation
	
Serveur
- SSH
	- clonez ce dépôt git. `git clone *nom-du-depot*`
	- `cd generateur_art_libre`
	- modifiez le fichier .env avec l'url souhaitée. Renseignez également un mot de passe.
	- Rendez l'utilisateur du serveur web propriétaire du dossier *captures* (www-data sur debian et ubuntu) \
	`chown -R www-data:www-data website/webroot/captures/`

	Pour ceux qui utilisent nginx, un exemple de fichier de configuration est présent dans le dossier *website/*

- FTP \
glissez le contenu du dossier *server/webroot* dans le dossier racine de votre site (généralement appelé *www*)